# Conception du projet

## Moule à Gaufre

## Déscription du projet

This project is to create a software intented to schools and educationnal establisment. Users would be school administrator, teachers, students and their famillies. Either teachers and students would be able to have an interface where they could see informations like, gardes, messages, agenda, homeworks. 

Le but de ce projet est de créer un logiciel  scolaire qui permet aux elèves et à leur famille d'avoir accès à un portail permettant de voir les informations fournis par le corps enseignants, tel que les notes, leurs agenda de cours, des messages. 

## Technologie utilisées :

    * REACT 
    * Node 
    * MongoDB

## Utilisateurs

    * Administration
    * Professeurs
    * Elèves / Famille

## Fonctionnalité (User Story)

* Administration : 

    The personn running as administrator (one or many), can see all the profils neither professor or students. The main point of this administrator is to create new profil, teacher/students or even a new class. It can assign teachers to a specific class. And the other point is to send messages to a single student or all students. And it is the one who an create lesson planning's.

    Il peut voir l'ensemble des profils (professeurs/élèves). Il est le seul à pouvoir ajouter des profils. Il peu ajouter de nouveaux professeurs, l'assigner à une classe, de plus c'est lui qui créer les classes. Il peut aussi envoyer des messages à n'importe quel utilisateur. La section vie scolaire lui correspond. De plus c'est lui qui créer les plannings.

* Professeur : 

    This user can have acess to his students profil's. He can also add homework from his interface. Their is a possibility for him to add some documents on the platform, those documents would be readble or downloadable form the students interface. He can see all of his uploads, with a delete option. Because he is a teacher, he can add some homework and tests on the student's agenda. He can grants grades, those grades would be link with the tests.

    Cet utilisateur peut voir le profil des élèves de sa classe. Il peut ajouter du travail à faire depuis son interface. Il peut aussi ajouter des documents sur la platefrome qui seront consultable par les élèves, il peut aussi voir l'ensemble des dessources qu'il a poster avec la possibilité des les supprimer si nécessaire. Il modifie l'agenda des élèves pour y ajouter des devoirs et/ou controles. Il a aussi la possibilité de mettre des notes aux élèves (la moyen sera une donnée calculé). 

* Elèves / Famille :
    Cet utilisateur peu voir son agenda de cours, le travail à faire, les ressources pédagogiques mise à disposition par les professeurs. Il voit aussi ses notes par matièreainsi que la moyenne (Ajout d'un diagramme montrant les points forts et faibles selon les notes). Il peut communiquer avec ses camarades via un tchat. Les messages de la vie scolaire appraissent en page d'accueil.


## Diagrame MCD

```mermaid
classDiagram
    professeur "1" -- "n" cours
    professeur "1" -- "n" ressource
    ressource "0,1" -- "1" travail
    professeur "1" -- "n" travail
    travail "n" -- "1" classe
    professeur "1" -- "n" controle
    controle "1" -- "n" EleveControle
    eleve "1" -- "n" EleveControle
    vieScolaire "n" -- "0,1" classe
    vieScolaire "n" -- "0,1" eleve
    classe "1" -- "n" cours
    classe "1" -- "n" eleve

    class eleve {
      idEleve : int PK
      idClasse: FK
      nomEleve : varchar
      prenomEleve : varchar
      dateNaissanceEleve : datetime
      adresseEleve: varchar
      emailParent: varchar
      telephoneParent: varchar
      loginEleve: varchar
      mdpEleve: varchar
      
    }
    class professeur{
      idProfesseur: int PK
      nomProfesseur: varchar
      prenomProfesseur: varchar
      dateNaissanceProfesseur: datetime
      emailProfesseur: varchar
      loginProfesseur: varchar
      mdpProfesseur: varchar
      matiereProfesseur: varchar
    }

    class administrateur{
    idAdmin: int Pk
    nomAdmin: varchar
    prenomAdmin: varchar
    loginAdmin: varchar
    mdpAdmin: varchar
    }

    class cours{
      idCours: int PK
      idProfesseur: FK
      idCLasse: FK
      nomCours: varchar
      dateCours: datetime
      horaireDebutCours: time
      horaireFinCours: time
    }

    class ressource{
      idRessource: int PK
      idProfesseur: FK
      nomRessource: varchar
      contenuRessource: fichier
      dateRessource: datetime
    }

    class travail{
      idTravail: int Pk
      idProfesseur: FK
      nomTravail:varchar
      enonceTravail: varchar
      dateTravail: datetime
      dateRenduTravail: datetime
      renduEleveTravail: varchar
    }
    class classe{
      idClasse: int Pk
      nomCLasse: VARCHAR
      
    }
    class controle{
      idControle: int Pk
      idProfesseur: FK
      titreControle: varchar
      dateControle: datetime
    }

    class EleveControle{
      idEleve: FK
      Idcontrole: FK
      note: VARCHAR
    }

    class vieScolaire{
        idVieScolaire: int Pk
        idClasse: FK NULL
        idEleve: FK NULL
        titreVieScolaire: varchar
        dateVieScolaire :datetime
        contenuVieScolaire: varchar
        heureVieScolaire: datetime
        utilisateurVieScolaire: varchar
    }
```

## Idées supplémentaires :
    
    * Administration/Professeur : Possibilité d'envoyer une notification sur mail/portable des parents d'élèves.
    
