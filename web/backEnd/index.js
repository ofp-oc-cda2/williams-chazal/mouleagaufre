const mongoose = require('mongoose')
const express = require('express')
const app = express();


require('./models/dbConfig');

const classRoutes = require('./routes/classesController');
const bodyParser = require('body-parser');

app.get("/", (req, res) => {
    res.json({ message: "Bienvenue sur le projet Moule a gaufre." });
  });


app.use(bodyParser.json());
app.use('/classes', classRoutes)


const PORT = process.env.PORT || 8080;
app.listen(PORT, () => {
    console.log(`Server is running on port ${PORT}.`);
});
  