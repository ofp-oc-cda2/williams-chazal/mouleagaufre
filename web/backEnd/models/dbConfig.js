const mongoose = require('mongoose');

mongoose.connect(
    "mongodb://localhost:27017/projectMAG",
    { useNewUrlParser: true, useUnifiedTopology: true },
    (err) => {
        if(!err) console.log ("Connected to DataBase");
        else console.log("COnnection error : " + err);
    }
)