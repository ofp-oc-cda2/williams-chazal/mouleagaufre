const mongoose = require('mongoose');

const classModel = mongoose.model(
    "projectMAG",
    {
        className:{
            type: String,
            required : true
        },
        nbrStud:{
            type: Number,
            required: true
        }
    },
    "classes"
)

module.exports = { classModel };