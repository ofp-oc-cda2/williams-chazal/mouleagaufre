const express = require('express');
const router = express.Router();
const ObjectID = require('mongoose').Types.ObjectId

const { classModel } = require('../models/classModel');

router.get('/', (req, res) => {
    classModel.find((err, docs) => {
        if (!err) res.send(docs)
        else console.log('Error getting data :' + err);
    })
})

router.get('/', (req, res) => {
    classModel.find((err, docs) => {
        if (!err) res.send(docs)
        else console.log('Error getting data :' + err);
    })
})

// Add a new class
router.post('/', (req, res) => {
    const newClass = new classModel({
        className: req.body.className,
        nbrStud: req.body.nbrStud
    });

    newClass.save((err, docs) => {
        if (!err) res.send(docs)
        else console.log('Error creating new data :' + err)
    })
})


//Find a class by ID
router.get('/:id', (req, res) => {
    classModel.find((err, docs) => {
        if (!ObjectID.isValid(req.params.id))
        return res.status(400).send("ID unknow : " + req.params.id)
        
    })
    const id = {
        className: req.body.className, 
        nbrStud: req.body.nbrStud
    };

    classModel.findById(
        req.params.id,
        (err, docs) => {
            if (!err) res.send(docs);
            else console.log("Update error : " + err);
        }
    )
})

//Find a class by className
/* router.get('/:className', (req, res) => {
    classModel.find((err, docs) => {

}) */



// delete a class by name


// delete a class by id
router.delete('/:id', (req, res) => {
    if (!ObjectID.isValid(req.params.id))
    return res.status(400).send("ID unknow : " + req.params.id)

    classModel.findByIdAndDelete(
        req.params.id,
        (err, docs) => {
            if (!err) res.send(docs);
            else console.log ("Delete error : " + err)
        }
    )
})

module.exports = router;